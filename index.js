import Waterfall from './src/Waterfall.js';

exports = {
	Waterfall: Waterfall
};

// CommonJS
module.exports = exports;
// ES 6
export default exports;
